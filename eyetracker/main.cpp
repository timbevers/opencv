/**
* Bevers Tim
*
*
* http://docs.opencv.org/3.1.0/d7/d66/tutorial_feature_detection.html
* http://docs.opencv.org/3.1.0/d5/dde/tutorial_feature_description.html
* http://docs.opencv.org/3.1.0/d7/dff/tutorial_feature_homography.html
*/

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

const char* result_window = "Result window";

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image i | | (required) image }"
                             "{ obj o | | (required) object }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }
    string parameter2(parser.get<string>("obj"));
    if (parameter2.empty())
    {
        parser.printMessage();
        return -1;
    }

    namedWindow( result_window, WINDOW_AUTOSIZE );
    Mat img_matches;

    Mat img = imread(parser.get<string>("image"),IMREAD_GRAYSCALE);
    Mat obj = imread(parser.get<string>("obj"),IMREAD_GRAYSCALE);
    Mat res_img, res_obj;

    /// Opgave 1
    std::vector<KeyPoint> kp_img, kp_obj;
    //Ptr<FeatureDetector> detector = ORB::create();
    //Ptr<FeatureDetector> detector = BRISK::create();
    Ptr<FeatureDetector> detector = AKAZE::create();
    Mat descriptors_img, descriptors_obj;
    detector->detectAndCompute( img, Mat(), kp_img, descriptors_img );
    detector->detectAndCompute( obj, Mat(), kp_obj, descriptors_obj );

    /// Opgave 2

    /// Matching descriptor vectors with a brute force matcher
    BFMatcher matcher(NORM_L2);
    std::vector< DMatch > matches;
    matcher.match( descriptors_obj, descriptors_img, matches );
    /// Draw matches

    drawMatches( obj, kp_obj, img, kp_img, matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS  );
    /// Show detected matches
    imshow(result_window, img_matches );
    waitKey();



    double max_dist = 0;
    double min_dist = 100000;
    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptors_obj.rows; i++ )
    {
        double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }
    printf("-- Max dist : %f \n", max_dist );
    printf("-- Min dist : %f \n", min_dist );
    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;
    for( int i = 0; i < descriptors_obj.rows; i++ )
    {
        if( matches[i].distance < 3*min_dist )
        {
            good_matches.push_back( matches[i]);
        }
    }

    drawMatches( obj, kp_obj, img, kp_img, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS  );
    /// Show detected matches
    imshow(result_window, img_matches );
    waitKey();

    //-- Localize the object
    std::vector<Point2f> obj_loc;
    std::vector<Point2f> img_loc;
    for( size_t i = 0; i < good_matches.size(); i++ )
    {
        //-- Get the keypoints from the good matches
        obj_loc.push_back( kp_obj[ good_matches[i].queryIdx ].pt );
        img_loc.push_back( kp_img[ good_matches[i].trainIdx ].pt );
    }
    Mat H = findHomography( obj_loc, img_loc, RANSAC );

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0,0);
    obj_corners[1] = cvPoint( obj.cols, 0 );
    obj_corners[2] = cvPoint( obj.cols, obj.rows );
    obj_corners[3] = cvPoint( 0, obj.rows );
    std::vector<Point2f> scene_corners(4);
    perspectiveTransform( obj_corners, scene_corners, H);
    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    line( img_matches, scene_corners[0] + Point2f( obj.cols, 0), scene_corners[1] + Point2f( obj.cols, 0), Scalar( 0, 255, 0), 2 );
    line( img_matches, scene_corners[1] + Point2f( obj.cols, 0), scene_corners[2] + Point2f( obj.cols, 0), Scalar( 0, 255, 0), 2 );
    line( img_matches, scene_corners[2] + Point2f( obj.cols, 0), scene_corners[3] + Point2f( obj.cols, 0), Scalar( 0, 255, 0), 2 );
    line( img_matches, scene_corners[3] + Point2f( obj.cols, 0), scene_corners[0] + Point2f( obj.cols, 0), Scalar( 0, 255, 0), 2 );
    //-- Show detected matches
    imshow(result_window, img_matches );

    waitKey();
    return 0;
}
