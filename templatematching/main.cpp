#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

const char* image_window = "Source Image";
const char* result_window = "Result window";

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image i | | (required) image }"
                             "{ template t | | (required) template }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }
    string parameter2(parser.get<string>("template"));
    if (parameter2.empty())
    {
        parser.printMessage();
        return -1;
    }

    namedWindow( image_window, WINDOW_AUTOSIZE );
    namedWindow( result_window, WINDOW_AUTOSIZE );

    Mat img = imread(parser.get<string>("image"),IMREAD_COLOR);
    Mat templ = imread(parser.get<string>("template"),IMREAD_COLOR);

    Mat res;
    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;
    res.create( result_rows, result_cols, CV_32FC1 );

    matchTemplate( img, templ, res, TM_SQDIFF );
    normalize( res, res, 0, 1, NORM_MINMAX, -1, Mat() );
    /// TM_SQDIFF geef minima voor matches - inverteren voor maxima
    res = 1-res;

    /// Thesholding om van locale maxima de contour en dan boundingbox te bepalen
    Mat thr;
    threshold(res.clone(), thr, 0.9, 1, CV_THRESH_BINARY);
    res = res & thr;/// zorgen dat er nog locale maxima zijn


    double minVal;
    double maxVal;
    Point minLoc;
    Point maxLoc;
    Point matchLoc;
    Mat conv;
    res.convertTo(conv, CV_8UC1); /// findContours werkt met CV_8UC1
    vector<vector<Point> > contours;
    findContours( conv, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE );

    for( size_t i = 0; i< contours.size(); i++ )
    {
        //drawContours( res, contours, i, Scalar(200,200,200), 2, FILLED);
        Rect bRect = boundingRect( Mat(contours[i]));
        Mat temp = res(bRect);
        //rectangle( res, bRect.tl(), bRect.br() , Scalar(255,255,255), 2, 8, 0 );

        /// Locale minMax per boundingbox
        minMaxLoc( temp, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
        matchLoc = maxLoc;

        /// Locatie van de boundingbox bij de relatieve locatie optellen
        rectangle( img, matchLoc + bRect.tl(), Point( matchLoc.x + templ.cols , matchLoc.y + templ.rows ) + bRect.tl(), Scalar(200,200,0), 2, 8, 0 );

        //imshow( result_window, res );
        //waitKey();

    }

    imshow(image_window, img);
    imshow( result_window, res );

    waitKey();
    return 0;
}
