/**
* Bevers Tim
*
*/

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
using namespace ml;

const char* result_window = "Result window";


void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{

    vector<Point>* points = (vector<Point>*) userdata;

    /// EVENT_LBUTTONDOWN, EVENT_RBUTTONDOWN, EVENT_MBUTTONDOWN, EVENT_MOUSEMOVE
    if  ( event == EVENT_LBUTTONDOWN )
    {
        cout << "[" << x << "," << y << "]" << endl;
        ///push_back
        points->push_back(Point(x,y));
    }
    else if  ( event == EVENT_RBUTTONDOWN )
    {
        //cout << "Right button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
        ///pop_back
        if( points->size() > 0 )
        {
            points->pop_back();
        }
    }
    else if  ( event == EVENT_MBUTTONDOWN )
    {
        //cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
        for( unsigned i = 0; i < points->size(); i++)
        {
            cout << "[" << points->at(i).x  << "," << points->at(i).y  << "] ";

        }
        cout << endl;
    }
}

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image i | | (required) image }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    Mat img = imread(parser.get<string>("image"));

    namedWindow(result_window, WINDOW_AUTOSIZE);

    vector<Point> strawberrys;

    ///set the callback function for any mouse event
    setMouseCallback(result_window, CallBackFunc, (void*)(&strawberrys));

    imshow(result_window, img );
    waitKey();

    vector<Point> background;

    ///set the callback function for any mouse event
    setMouseCallback(result_window, CallBackFunc, (void*)(&background));

    imshow(result_window, img );
    waitKey();

    ///Gausianblur 5 5 -- zodat je geen "slechte pixel aanduid"
    Mat work;
    GaussianBlur( img, work, Size( 5, 5 ), 0, 0 );

    ///hsv
    cvtColor( work, work, COLOR_BGR2HSV );

    ///vec3b
    vector<Vec3b> descriptors;

    for( unsigned i = 0; i < strawberrys.size(); i ++)
    {
        descriptors.push_back( work.at<Vec3b>(strawberrys[i].x,strawberrys[i].x));
    }
    for( unsigned i = 0; i < background.size(); i ++)
    {
        descriptors.push_back( work.at<Vec3b>(background[i].x,background[i].x));
    }

    Mat lables(descriptors.size(), 1, CV_32SC1);
    Mat trainingData(descriptors.size(), 3, CV_32FC1);

    for( unsigned i = 0; i < descriptors.size(); i ++)
    {
        Vec3b temp = descriptors[i];

        trainingData.at<float>(i,0) = temp[0];
        trainingData.at<float>(i,1) = temp[1];
        trainingData.at<float>(i,2) = temp[2];
        if( i < strawberrys.size() )
        {
            lables.at<int>(i,0) = 1;
        }
        else
        {
            lables.at<int>(i,0) = 0;
        }
    }

    cout << trainingData << endl;
    cout << lables << endl;

    ///kNN
    Ptr<KNearest> kNN = KNearest::create();
    Ptr<TrainData> trainingDataKNN = TrainData::create(trainingData, ROW_SAMPLE, lables);
    kNN->setIsClassifier(true);
    kNN->setAlgorithmType(KNearest::BRUTE_FORCE);
    kNN->setDefaultK(1);
    kNN->train(trainingDataKNN);

    ///NBC
    Ptr<NormalBayesClassifier> NBC = NormalBayesClassifier::create();
    NBC->train(trainingData, ROW_SAMPLE, lables);

    ///SVM
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));
    svm->train(trainingData, ROW_SAMPLE, lables);

    Mat data_test(1, 3, CV_32FC1);
    Mat labels_kNN, labels_NBC, labels_SVM, distance_kNN;
    Mat mask_kNN = Mat::zeros( img.rows, img.cols, CV_8UC1), mask


    waitKey();
    return 0;

    ///Extra R - G
}
