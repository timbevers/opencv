#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void part1(Mat img)
{
    /// Regel voor huidskleur
    /// (RED>95) && (GREEN>40) && (BLUE>20) && ((max(RED,max(GREEN,BLUE)) - min(RED, min(GREEN,BLUE)))>15) && (abs(RED-GREEN)>15) && (RED>GREEN) && (RED>BLUE);

    imshow("image", img);

    /// Splitsen van afbeelding in 3 kleuren [B]lauw, [G]roen, [R]ood.
    Mat bgr[3]; /// Image matrix met 3 kanalen.
    split(img, bgr);

    Mat thr = Mat::zeros(img.rows, img.cols, CV_8U);/// Nieuwe lege matrix
    Mat thr_col = Mat::zeros(img.rows, img.cols, CV_8UC3);/// Nieuwe lege matrix

    for( int row = 0; row < thr.rows; row++ )
    {
        for( int col = 0; col < thr.cols; col++ )
        {
            uchar blue = bgr[0].at<uchar>(row,col);
            uchar green = bgr[1].at<uchar>(row,col);
            uchar red = bgr[2].at<uchar>(row,col);
            if((red>95) && (green>40) && (blue>20) && ((max(red,max(green,blue)) - min(red, min(green,blue)))>15) && (abs(red-green)>15) && (red>green) && (red>blue))
            {
                thr.at<uchar>(row,col) = 255;
                thr_col.at<Vec3b>(row,col) = img.at<Vec3b>(row,col);
            }
        }
    }
    imshow("gefilterd op huidskkleur", thr);

    /// Huidskleur uit de afbeelding gehaald
    imshow("gefilterd op huidskkleur in kleur", thr_col);
}

void part2(Mat img)
{
    Mat img_gs,res,hist,cla;
    cvtColor( img, img_gs, COLOR_BGR2GRAY );
    resize(img_gs, img_gs, Size(img.cols/4, img.rows/4));
    imshow("grayscale image", img_gs);

    threshold(img_gs, res, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    imshow("thresholded image", res);

    //adaptiveThreshold(img_gs,res,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,11,14 );
    //imshow("adaptiveThreshold image", res);

    equalizeHist( img_gs.clone(), hist );
    threshold(hist, res, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    imshow("thresholded image + hist", res);

    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(1);
    clahe->setTilesGridSize(Size(15,15));

    clahe->apply(img_gs.clone(),cla);
    threshold(cla, res, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    imshow("clahe", res);

}


int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image1 i1 | | (required) image part 1 }"
                             "{ image2 i2 | | (required) image part 2 }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image1"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    string parameter2(parser.get<string>("image2"));
    if (parameter2.empty())
    {
        parser.printMessage();
        return -1;
    }

    /// Inlezen van de afbeelding in kleur.
    Mat img = imread(parser.get<string>("image1"),IMREAD_COLOR);

    part1(img);

    img = imread(parser.get<string>("image2"),IMREAD_COLOR);

    part2(img);


    waitKey();
    return 0;
}




