/**
* Bevers Tim
*
*/

#include <iostream>
#include <opencv2/opencv.hpp>

#include <time.h>

using namespace std;
using namespace cv;
using namespace cv::ml;

const char* window_one = "1";
const char* window_two = "2";

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image1 i1 | | (required) image }"
                             "{ image2 i2 | | (required) image }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image1"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    /// Collect data from arguments
    string parameter2(parser.get<string>("image2"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    Mat im1 = imread(parser.get<string>("image1"));
    Mat im2 = imread(parser.get<string>("image2"));

    namedWindow( window_one, WINDOW_AUTOSIZE );
    namedWindow( window_two, WINDOW_AUTOSIZE );

    Mat im1_hsv, im2_hsv;
    cvtColor(im1, im1_hsv, COLOR_BGR2HSV );
    cvtColor(im2, im2_hsv, COLOR_BGR2HSV );

    imshow(window_one, im1 );
    imshow(window_two, im2 );

    clock_t start, endt;
    double elapsed;
    start = clock();

    std::vector<cv::Rect> found;
    cv::HOGDescriptor hog;
    hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
    hog.detectMultiScale(im1_hsv, found, 0, cv::Size(8,8), cv::Size(32,32), 1.05, 2);

    endt = clock();
    elapsed = ((double) (endt - start)) / CLOCKS_PER_SEC * 100;
    std::cout<< "Functie duur: " << elapsed << " ms" << endl;

    cout << "Found:" << endl;
    for(unsigned i=0; i<found.size(); i++)
    {
        cout << found[i] << endl;
        rectangle(im1, found[i], Scalar( 0, 255 ,0 ), 5);
    }

    int h_bins = 50;
    int s_bins = 60;
    int histSize[] = { h_bins, s_bins };

    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    int channels[] = { 0, 1 };

    start = clock();

    for(unsigned i=0; i<found.size(); i++)
    {

        Mat person = im1_hsv( found[i] );

        Mat hist;
        calcHist( &person, 1, channels, Mat(), hist, 2, histSize, ranges, true, false );
        normalize( hist, hist, 0, 1, NORM_MINMAX, -1, Mat() );

        double max_found = 0;
        int max_j = 0, max_k = 0;
        for(int j=0; j < (im2_hsv.rows - person.rows ); j++)
        {
            for(int k=0; k < (im2_hsv.cols - person.cols -5); k++)
            {
                Rect roi = Rect(k, j, person.cols, person.rows);
                Mat hsv2ROI = im2_hsv(roi);

                Mat hist2;
                calcHist( &hsv2ROI, 1, channels, Mat(), hist2, 2, histSize, ranges, true, false );
                normalize( hist2, hist2, 0, 1, NORM_MINMAX, -1, Mat() );

                double found_hist = compareHist( hist2, hist, CV_COMP_CORREL );

                if (found_hist > max_found)
                {
                    max_found = found_hist;
                    max_j = j;
                    max_k = k;
                }
            }
        }

        Rect roi = Rect(max_k, max_j, person.cols, person.rows);
        rectangle(im2, roi, Scalar( 255, 255 ,255 ), 1, LINE_8, 0 );

    }

    endt = clock();
    elapsed = ((double) (endt - start)) / CLOCKS_PER_SEC * 100;
    std::cout<< "Color histogram: " << elapsed << " ms" << endl;


    imshow(window_one, im1 );
    imshow(window_two, im2 );

    waitKey();
    return 0;
}
