#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/// Global Variables
const int slider_max = 255;
int slider;

Mat img;
Mat res;

void on_trackbar( int, void* )
{
    int test = (double) slider/slider_max ;

    Mat hsv = res;
    Mat res_hsv;

    Mat mask1, mask2;
    /// in, min, max, uit
    inRange(hsv, Scalar(0, 70, 0), Scalar(10, 255, 255), mask1);
    inRange(hsv, Scalar(170, 70, 0), Scalar(180, 255, 255), mask2);

    res_hsv = mask1 | mask2;

    erode(res_hsv.clone(), res_hsv, Mat(5,5,CV_8UC1), Point(-1, -1), 1);
    dilate(res_hsv.clone(), res_hsv,Mat(5,5,CV_8UC1), Point(-1, -1), 2);
    erode(res_hsv.clone(), res_hsv, Mat(3,3,CV_8UC1), Point(-1, -1), 1);

    imshow("inrange image", res_hsv);

    vector<vector<Point> > contours;
    findContours( res_hsv, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE );
    Mat con = img.clone();
    for( size_t i = 0; i< contours.size(); i++ )
    {
        drawContours( con, contours, i, Scalar(0,200,255), 2, FILLED);
    }
    imshow("contour on image", con);
}

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image i | | (required) image }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    img = imread(parser.get<string>("image"),IMREAD_COLOR);

    imshow("input image", img);

    /// Splitsen van afbeelding in 3 kleuren [B]lauw, [G]roen, [R]ood.
    /*Mat bgr[3]; /// Image matrix met 3 kanalen.
    split(img, bgr);
    Mat red = bgr[1];

    imshow("blue layer of image", bgr[0]);
    imshow("green layer of image", bgr[1]);
    imshow("red layer of image", bgr[2]);

    Mat res;
    threshold(bgr[0], res, 128, 255, CV_THRESH_BINARY_INV);
    imshow("blue thresholded image", res);
    threshold(bgr[1], res, 128, 255, CV_THRESH_BINARY_INV);
    imshow("green thresholded image", res);
    threshold(bgr[2], res, 128, 255, CV_THRESH_BINARY);
    imshow("red thresholded image", res);
    */

    /// HSV
    Mat hsv,res_hsv;
    cvtColor(img, hsv, COLOR_BGR2HSV);

    res = hsv;

    //imshow("input image hsv", hsv);

    /*Mat mask1, mask2;
    /// in, min, max, uit
    inRange(hsv, Scalar(0, 70, 0), Scalar(10, 255, 255), mask1);
    inRange(hsv, Scalar(170, 70, 0), Scalar(180, 255, 255), mask2);

    res_hsv = mask1 | mask2;

    erode(res_hsv.clone(), res_hsv, Mat(5,5,CV_8UC1), Point(-1, -1), 1);
    dilate(res_hsv.clone(), res_hsv,Mat(5,5,CV_8UC1), Point(-1, -1), 2);
    erode(res_hsv.clone(), res_hsv, Mat(3,3,CV_8UC1), Point(-1, -1), 1);

    imshow("inrange image", res_hsv);

    vector<vector<Point> > contours;
    findContours( res_hsv, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE );
    Mat con = img.clone();
    for( size_t i = 0; i< contours.size(); i++ )
    {
        drawContours( con, contours, i, Scalar(0,200,255), 2, FILLED);
    }
    imshow("contour on image", con);*/

    /// Trackbars
    namedWindow("contour on image", 1);
    createTrackbar( "tb", "contour on image", &slider, slider_max, on_trackbar );
    on_trackbar(slider, 0);
    waitKey();
    return 0;
}
