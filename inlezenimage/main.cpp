#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, const char **argv)
{

    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image i | | (required) image name }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    //cout << "Hello world!" << endl;

    /// Inlezen van de afbeelding in kleur.
    Mat img = imread(parser.get<string>("image"),IMREAD_COLOR);

    /// Weergeven van de afbeelding met de naam "image".
    imshow("image", img);
    waitKey(0); /// Wachten op user input.

    //destroyAllWindows();

    /// Splitsen van afbeelding in 3 kleuren [B]lauw, [G]roen, [R]ood.
    Mat bgr[3]; /// Image matrix met 3 kanalen.
    split(img, bgr);
    imshow("Blue", bgr[0]);
    imshow("Green", bgr[1]);
    imshow("Red", bgr[2]);
    waitKey(0);

    /// Omzetten kleur naar grijswaarden.
    Mat img_gs;
    cvtColor( img, img_gs, COLOR_BGR2GRAY );
    imshow("grayscale image", img_gs);
    waitKey(0);

    for( int row = 0; row < img_gs.rows; row++ )
    {
        for( int col = 0; col < img_gs.cols; col++ )
        {
            cout << (int)img_gs.at<uchar>(row,col) << " ";
        }
        cout << endl;
    }

    /// Nieuwe lege matrix
    Mat canvas = Mat::zeros(200, 200, CV_8U);
    circle(canvas, Point(50,50), 20, Scalar(100, 100, 100), 2);
    line(canvas, Point(50,50), Point(100,100),Scalar(150, 100, 110), 2 );

    imshow("canvas", canvas);

    waitKey(0);
    return 0;
}
