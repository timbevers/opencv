/**
* Bevers Tim
*
*/

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

const char* result_window = "Result window";
const char* left_window = "Left window";
const char* right_window = "Right window";

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
    Point * point = (Point*) userdata;

    /// EVENT_LBUTTONDOWN, EVENT_RBUTTONDOWN, EVENT_MBUTTONDOWN, EVENT_MOUSEMOVE
    if  ( event == EVENT_LBUTTONDOWN )
    {
        cout << "[" << x << "," << y << "]" << endl;
        point->x = x;
        point->y = y;
    }
}

int main(int argc, const char **argv)
{
    /// Adding a little help option and command line parser input
    CommandLineParser parser(argc, argv,
                             "{ help h usage ? | | show this message }"
                             "{ image1 i1 | | (required) image }"
                             "{ image2 i2 | | (required) image }"
                            );

    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }

    /// Collect data from arguments
    string parameter(parser.get<string>("image1"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }

    /// Collect data from arguments
    string parameter2(parser.get<string>("image2"));
    if (parameter.empty())
    {
        parser.printMessage();
        return -1;
    }


    FileStorage fs("mx1.xml", FileStorage::READ);
    Mat mx1;
    fs["mx1"] >> mx1;
    fs.release();

    fs.open("my1.xml", FileStorage::READ);
    Mat my1;
    fs["my1"] >> my1;
    fs.release();

    fs.open("mx2.xml", FileStorage::READ);
    Mat mx2;
    fs["mx2"] >> mx2;
    fs.release();

    fs.open("my2.xml", FileStorage::READ);
    Mat my2;
    fs["my2"] >> my2;
    fs.release();

    Mat imL = imread(parser.get<string>("image1"));
    Mat imR = imread(parser.get<string>("image2"));

    namedWindow( result_window, WINDOW_AUTOSIZE );
    namedWindow( left_window, WINDOW_AUTOSIZE );
    namedWindow( right_window, WINDOW_AUTOSIZE );

    //imshow(left_window, imL );
    //imshow(right_window, imR );

    remap( imL, imL, mx1, my1, INTER_LINEAR );
    remap( imR, imR, mx2, my2, INTER_LINEAR );

    //waitKey();
    imshow(left_window, imL );
    imshow(right_window, imR );

    Point pointL;
    //Point pointR;
    setMouseCallback(left_window, CallBackFunc, (void*)(&pointL));
    //setMouseCallback(right_window, CallBackFunc, (void*)(&pointR));
    waitKey();

    int tmplsize = 30;
    int variantie = 5;
    Rect roi(pointL.x-tmplsize, pointL.y-tmplsize, tmplsize*2, tmplsize*2);
    cout << roi << endl;
    rectangle( imL, roi, Scalar(200,200,0), 1, 8, 0 );
    Mat templ = imL( roi );

    Rect hline(0, pointL.y - (tmplsize + variantie), imR.cols, (tmplsize + variantie)*2 );
    rectangle( imR, hline, Scalar(200,200,0), 1, 8, 0 );
    cout << hline << endl;

    imshow(left_window, imL );
    imshow(right_window, imR );

    Mat res;
    matchTemplate( imR(hline), templ, res, TM_SQDIFF );
    normalize( res, res, 0, 1, NORM_MINMAX, -1, Mat() );
    /// TM_SQDIFF geef minima voor matches - inverteren voor maxima
    res = 1-res;
    imshow(result_window, res );


    double minVal, maxVal;
    Point minLoc, maxLoc, matchLoc;
    minMaxLoc( res, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
    matchLoc = maxLoc;
    matchLoc.x += tmplsize;
    matchLoc.y = pointL.y;

    /// B/Z = (x-x')/f -> Z

    float B = 12;///cm
    float f = 742.2;///pixels
    float x1 = pointL.x - (imL.cols/2);
    //float x2 = pointR.x - (imL.cols/2);
    float x2 = matchLoc.x  - (imL.cols/2);
    float Z = (B*f)/(x1-x2); /// - Halve breedte van een image (imL.cols/2) tellen tot center

    cout << Z << "cm" << endl;
    circle(imR, matchLoc , 10, Scalar(200,200,0));

    imshow(left_window, imL );
    imshow(right_window, imR );

    waitKey();
    return 0;
}
